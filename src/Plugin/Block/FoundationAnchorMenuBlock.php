<?php

namespace Drupal\foundation_anchor_menu_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Provides a dynamic 'Foundation Anchor Menu' Block.
 *
 * @Block(
 *   id = "foundation_anchor_menu_block_block",
 *   admin_label = @Translation("Foundation Anchor Menu Block (Dynamic by JavaScript)"),
 *   admin_description = @Translation("Only visible if at least one anchor menu item exists on the current page. Content created at runtime by JavaScript."),
 *   category = @Translation("Navigation"),
 * )
 */
class FoundationAnchorMenuBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'famb_anchor_menu',
      '#menu_orientation' => $this->configuration['menu_orientation'],
      '#menu_horizontal_breakpoint' => $this->configuration['menu_horizontal_breakpoint'],
      '#list_classes_additional' => array_unique(explode(' ', $this->configuration['list_classes_additional'])),
      '#attached' => [
        'library' => [
          'foundation_anchor_menu_block/famb-anchor-menu',
        ],
        'drupalSettings' => [
          'foundation_anchor_menu_block' => [
            'menu_orientation' => $this->configuration['menu_orientation'],
            'menu_horizontal_breakpoint' => $this->configuration['menu_horizontal_breakpoint'],
            'list_classes_additional' => $this->configuration['list_classes_additional'],
            'animation_duration' => $this->configuration['animation_duration'],
            'animation_easing' => $this->configuration['animation_easing'],
            'threshold' => $this->configuration['threshold'],
            'active_class' => $this->configuration['active_class'],
            'active_body_class' => $this->configuration['active_body_class'],
            'deeplinking' => $this->configuration['deeplinking'],
            'update_history' => $this->configuration['update_history'],
            'offset_top_bar_selector' => $this->configuration['offset_top_bar_selector'],
            'offset' => $this->configuration['offset'],
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'menu_orientation' => 'horizontal',
      'menu_horizontal_breakpoint' => 'none',
      'list_classes_additional' => '',
      'animation_duration' => 500,
      'animation_easing' => 'linear',
      'threshold' => 50,
      'active_class' => 'is-active',
      'active_body_class' => 'anchor-menu-active',
      'deeplinking' => FALSE,
      'update_history' => FALSE,
      'offset_top_bar_selector' => '.top-bar:first',
      'offset' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    $form['menu_orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu orientation (on large devices)'),
      '#options' => [
        'vertical' => $this->t('Vertical'),
        'horizontal' => $this->t('Horizontal'),
      ],
      '#required' => TRUE,
      '#default_value' => $config['menu_orientation'] ?? 'horizontal',
      '#description' => $this->t('See <a href="@foundation_docs_vertical_menu_href" target="_blank">Foundation Docs: Vertical Menu</a>', ['@foundation_docs_vertical_menu_href' => 'https://get.foundation/sites/docs/menu.html#vertical-menu']),
    ];
    $form['menu_horizontal_breakpoint'] = [
      '#type' => 'select',
      '#title' => $this->t('Large device breakpoint'),
      '#options' => [
        'small' => $this->t('Small'),
        'medium' => $this->t('Medium') . ' (' . $this->t('Default') . ')',
        'large' => $this->t('Large'),
      ],
      '#required' => TRUE,
      '#default_value' => $config['menu_horizontal_breakpoint'] ?? 'medium',
      '#description' => $this->t('If the screen is larger than or equal to this breakpoint, the mobile dropdown will be converted to a regular menu.'),
    ];
    $form['list_classes_additional'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional list classes'),
      '#required' => FALSE,
      '#default_value' => $config['list_classes_additional'] ?? '',
      '#description' => $this->t('Adds further custom classes to the list (&lt;ul&gt;) element. Separated by space.'),
    ];

    // Magellan settings:
    // @todo Put in a field group.
    $form['animation_duration'] = [
      '#title' => $this->t('Animation duration'),
      '#type' => 'number',
      '#step' => 1,
      '#min' => 0,
      '#size' => 5,
      '#required' => TRUE,
      '#default_value' => $config['animation_duration'] ?? 500,
      '#description' => $this->t('Amount of time, in ms, the animated scrolling should take between locations. Default: 500'),
    ];
    $form['animation_easing'] = [
      '#title' => $this->t('Animation easing'),
      '#type' => 'select',
      '#options' => [
        'linear' => $this->t('Linear'),
        'swing' => $this->t('Swing'),
      ],
      '#required' => TRUE,
      '#default_value' => $config['animation_easing'] ?? 'linear',
      '#description' => $this->t('Animation style to use when scrolling between locations. Can be `swing` or `linear`.  Default: linear'),
    ];
    $form['threshold'] = [
      '#title' => $this->t('Threshold'),
      '#type' => 'number',
      '#step' => 1,
      '#min' => 0,
      '#size' => 5,
      '#required' => TRUE,
      '#default_value' => $config['threshold'] ?? 50,
      '#description' => $this->t('Number of pixels to use as a marker for location changes. Default: 50'),
    ];
    $form['active_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Active class'),
      '#required' => TRUE,
      '#default_value' => $config['active_class'] ?? 'is-active',
      '#description' => $this->t('Class applied to the active locations link on the magellan container. Default: is-active'),
    ];
    $form['active_body_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Active body class'),
      '#required' => TRUE,
      '#default_value' => $config['active_body_class'] ?? 'anchor-menu-active',
      '#description' => $this->t('Class applied to the body, if the anchor menu is active on the page. Default: anchor-menu-active'),
    ];
    $form['deeplinking'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deeplinking'),
      '#required' => FALSE,
      '#default_value' => $config['deeplinking'] ?? FALSE,
      '#description' => $this->t('Allows the script to manipulate the url of the current page, and if supported, alter the history.  Default: FALSE'),
    ];
    $form['update_history'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update history'),
      '#required' => FALSE,
      '#default_value' => $config['update_history'] ?? FALSE,
      '#description' => $this->t('Update the browser history with the active link, if deep linking is enabled. Default: FALSE'),
    ];
    $form['offset_top_bar_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Offset (Topbar) selector'),
      '#required' => FALSE,
      '#default_value' => $config['offset_top_bar_selector'] ?? '.top-bar:first',
      '#description' => $this->t('jQuery Top bar selector. If given used to calculate the offset automatically. Leave emtpy to only use a manual offset value. Default: .top-bar:first'),
    ];
    $form['offset'] = [
      '#type' => 'number',
      '#step' => 1,
      '#title' => $this->t('Offset (additional)'),
      '#required' => FALSE,
      '#default_value' => $config['offset'] ?? 0,
      '#description' => $this->t('Number of pixels to offset the scroll of the page on item click if using a sticky nav bar. Summed up with offset_top_bar_selector.outerHeight().'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save our custom settings when the form is submitted.
    $this->configuration['menu_orientation'] = $form_state->getValue('menu_orientation');
    $this->configuration['menu_horizontal_breakpoint'] = $form_state->getValue('menu_horizontal_breakpoint');
    $this->configuration['list_classes_additional'] = Html::escape(trim($form_state->getValue('list_classes_additional')));
    // Magellan settings:
    $this->configuration['animation_duration'] = $form_state->getValue('animation_duration');
    $this->configuration['animation_easing'] = $form_state->getValue('animation_easing');
    $this->configuration['threshold'] = $form_state->getValue('threshold');
    $this->configuration['active_class'] = $form_state->getValue('active_class');
    $this->configuration['active_body_class'] = $form_state->getValue('active_body_class');
    $this->configuration['deeplinking'] = $form_state->getValue('deeplinking');
    $this->configuration['update_history'] = $form_state->getValue('update_history');
    $this->configuration['offset_top_bar_selector'] = $form_state->getValue('offset_top_bar_selector');
    $this->configuration['offset'] = $form_state->getValue('offset');
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $menu_orientation = $form_state->getValue('menu_orientation');
    if (!in_array($menu_orientation, ['vertical', 'horizontal'])) {
      $form_state->setErrorByName('menu_orientation', $this->t('Must be @vertical or @horizontal', [
        '@vertical' => $this->t('Vertical'),
        '@horizontal' => $this->t('Horizontal'),
      ]));
    }
    $menu_horizontal_breakpoint = $form_state->getValue('menu_horizontal_breakpoint');
    if (!in_array($menu_horizontal_breakpoint, ['small', 'medium', 'large'])) {
      $form_state->setErrorByName('menu_horizontal_breakpoint', $this->t('Must be small, medium or large.'));
    }
  }

}

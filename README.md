# Foundation Anchor Menu Block

Provides [ZURB Foundation](https://get.foundation/) based dynamic anchor
menu blocks, dynamically generated based on contents of the current page
by classes and data attributes.

Allows flexible anchor / jump menus scrolling to specific page sections.
Especially helpful on One-page sites and for long contents.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/foundation_anchor_menu_block).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/foundation_anchor_menu_block).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [ZURB Foundation Sites](https://www.drupal.org/project/foundation_sites)
- [Entity Embed](https://www.drupal.org/project/entity_embed) + 
  [Entity Browser](https://www.drupal.org/project/entity_browser)
  (optional) to embed Anchor Blocks in CKEditor



## Installation

Important for Composer installation: See the composer installation instructions on 
`https://www.drupal.org/project/foundation_sites`
Also see [Downloading third-party libraries using Composer]
(https://www.drupal.org/docs/develop/using-composer/manage-dependencies#third-party-libraries)

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to your block configuration page and add the `"Dynamic Anchor Menu Block"`
2. Add anchor targets by block, custom markup, ... (see possibilities described beyond)


## Maintainers

- Julian Pustkuchen - [Anybody](https://www.drupal.org/u/anybody)
- Thomas Frobieter - [thomas.frobieter](https://www.drupal.org/u/thomasfrobieter)
- Joshua Sedler - [Grevil](https://www.drupal.org/u/grevil)

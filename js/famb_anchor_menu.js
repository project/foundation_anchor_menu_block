/**
 * @file
 * foundation_anchor_menu_block JavaScripts.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.foundation_anchor_menu_block = {
    attach: function (context, settings) {
      // Create namespace below Drupal for global variable access:
      Drupal.foundation_anchor_menu_block =
        typeof Drupal.foundation_anchor_menu_block !== "undefined"
          ? Drupal.foundation_anchor_menu_block
          : {};
      // Magellan settings (https://get.foundation/sites/docs/magellan.html#js-options) from block:
      var fambSettings =
        typeof settings.foundation_anchor_menu_block !== "undefined"
          ? settings.foundation_anchor_menu_block
          : false;
      if (!fambSettings) {
        // Escape, if no settings are provided.
        return;
      }

      // Further settings:
      // Calculate offset from topbar (.top-bar is a foundation standard), if given:
      var offset = parseInt(fambSettings.offset);
      if (
        fambSettings.offset_top_bar_selector.length > 0 &&
        $(fambSettings.offset_top_bar_selector).length > 0
      ) {
        offset =
          offset +
          parseInt($(fambSettings.offset_top_bar_selector).outerHeight());
      }

      var $fambAnchorMenu = $(once('famb-anchor-menu', "[data-famb-anchor-menu]", context));
      if (
        $fambAnchorMenu.length > 0 &&
        $("[data-anchor-menu-title]").length > 0
      ) {
        var $fambAnchorMenuDropdownPane = $fambAnchorMenu.parents(
          ".dropdown-pane:first"
        );
        var $fambAnchorMenuDropdownPaneToggleButton =
          $fambAnchorMenuDropdownPane.prev(
            ".button--anchor-menu-dropdown-toggle"
          );
        // TODO: The name horizontalMenuBreakpoint is misleading, its more like 'large breakpoint'
        var horizontalMenuBreakpoint = $fambAnchorMenu.data(
          "anchor-menu-horizontal-breakpoint"
        );
        var menuOrientation = ($fambAnchorMenu.data('anchor-menu-orientation')) ?? 'horizontal';
        $("body:first").addClass(fambSettings.active_body_class);
        $("[data-anchor-menu-title][data-magellan-target]", context).each(
          function () {
            var $section = $(this);
            var sectionId = $section.attr("id");
            var sectionTitle = $section.attr("data-anchor-menu-title");
            if (sectionTitle.length > 0) {
              $fambAnchorMenu.append(
                '<li><a href="#' + sectionId + '">' + sectionTitle + "</a></li>"
              );
            }
          }
        );
        // We have to initialize Magellan manually, otherwise the magellan navigation is not initialized correctly (using data-magellan doesn't work on dynamic menus).
        // Assign to Drupal.foundation_anchor_menu_block.magellan to make it accessible globally:
        Drupal.foundation_anchor_menu_block.magellan = new Foundation.Magellan(
          $fambAnchorMenu,
          {
            animationDuration: parseInt(fambSettings.animation_duration),
            animationEasing: fambSettings.animation_easing,
            threshold: parseInt(fambSettings.threshold),
            activeClass: fambSettings.active_class,
            deeplinking: fambSettings.deeplinking,
            updateHistory: fambSettings.update_history,
            offset: parseInt(offset),
          }
        );

        if (Foundation.MediaQuery.atLeast(horizontalMenuBreakpoint)) {
          // Large devices:
          // Remove the mobile versions foundation dropdown instance & the markup:
          if ($fambAnchorMenuDropdownPane.length) {
            $fambAnchorMenuDropdownPane.foundation("_destroy");
          }
          if (menuOrientation !== 'vertical') {
            $fambAnchorMenu.removeClass('vertical');
          }
          if ($fambAnchorMenu.parent().is($fambAnchorMenuDropdownPane)) {
            // TODO: This needs a better solution - so that we have an "undo". Maybe a clone of the magellan menu, one for mobile one for larger screens, so we can simply toggle the visibility.
            $fambAnchorMenu.unwrap();
            // also remove the toggle button
            $fambAnchorMenuDropdownPaneToggleButton.remove();
          }
        } else {
          // Small devices > set active section as dropdown toggle button text
          // Initally set first menu item as text
          var activeAnchorSectionName = $fambAnchorMenu.find("a:first").text();
          $fambAnchorMenuDropdownPaneToggleButton.text(activeAnchorSectionName);
          $fambAnchorMenu.addClass("vertical");
          $fambAnchorMenu.on("update.zf.magellan", function () {
            var scopedAnchorSectionName = $fambAnchorMenu
              .find("a.is-active")
              .text();
            if (
              scopedAnchorSectionName != activeAnchorSectionName &&
              scopedAnchorSectionName != ""
            ) {
              $fambAnchorMenuDropdownPaneToggleButton.text(
                scopedAnchorSectionName
              );
            } else {
              $fambAnchorMenuDropdownPaneToggleButton.text(
                activeAnchorSectionName
              );
            }
          });
          // Close dropdown pane if an anchor link is clicked
          $fambAnchorMenu.find("a").on("click", function () {
            $fambAnchorMenuDropdownPane.foundation("close");
          });
        }
        // Remove hidden classes => show anchor menu & dropdown toggle button (mobile)
        $fambAnchorMenu.removeClass("hidden");
        $fambAnchorMenuDropdownPaneToggleButton.removeClass("hidden");

        // Trigger Init Callback (and pass the initialised menu JQ object)
        $(document).trigger("famb:init", $fambAnchorMenu);
      }
    },
  };
})(jQuery, Drupal);

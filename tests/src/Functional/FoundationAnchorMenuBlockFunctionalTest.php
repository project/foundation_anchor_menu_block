<?php

namespace Drupal\Tests\foundation_anchor_menu_block\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods for testing foundation_anchor_menu_block.
 *
 * @group foundation_anchor_menu_block
 */
class FoundationAnchorMenuBlockFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'foundation_sites',
    'block',
    'block_test',
    'block_content',
    'embed',
    'entity_embed',
    'entity_browser',
    'test_page_test',
    'foundation_anchor_menu_block',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-foundation-anchor-menu-block');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests if the block is placeable.
   */
  public function testBlockIsPlaceable() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // See if it is possible to set a block in the header:
    $this->drupalGet('/admin/structure/block/add/foundation_anchor_menu_block_block/stark');
    $page->fillField('edit-region', 'header');
    $session->statusCodeEquals(200);
    $page->pressButton('edit-actions-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The block configuration has been saved.');
    $this->drupalGet('<front>');
    $session->elementExists('css', '#block-foundationanchormenublockdynamicbyjavascript');
  }

}
